#include <stdio.h>
#include <string.h>


void replace(int *a, int *b);

int main(void){

	
	int x = 4;
	int y = 6;

	replace(&x, &y);
	printf("%d %d\n", x, y);
}



void replace(int *a, int *b){
	
	*b = (*a) + (*b);
	*a = *b - *a;
	*b = *b - *a;	
	
		
}
