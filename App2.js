import React, { Component } from 'react';
import {  Platform, 
          StyleSheet, 
          Text, 
          View,
          Image
        } from 'react-native';

import axios from 'axios'

// IMPORT SCREENS
import Splash from './js/components/screens/Splash'
import SignUp from './js/components/screens/SignUp'
import LogIn from './js/components/screens/LogIn'
import BusinessAccountSignUp from './js/components/screens/BusinessAccountSignUp'

import {Router, Scene} from 'react-native-router-flux'


export default class App2 extends Component{

    constructor(props){
        super(props);

        this.state = {}
    }

    render(){
        return (
            <Router>
                <Scene key="root">
                    <Scene key="splash" component={Splash} 
                        navigationBarStyle={{display:'none'}}
                        initial
                    />
                    <Scene key="SignUp" component={SignUp}
                        navigationBarStyle={{display:'none'}}
                    back 
                    />
                    <Scene key="LogIn" component={LogIn}
                        navigationBarStyle={{display:'none'}}
                    back 
                    />
                    
                    <Scene key="BizAccount" component={BusinessAccountSignUp}
                        navigationBarStyle={{display:'none'}}
                    back     
                    />
                </Scene>
            </Router>
        );
    }
}