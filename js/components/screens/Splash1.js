import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';



import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        Image } from 'react-native';
 
import styled from 'styled-components'


import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'


/*Styles*/

import {
    MainContainer,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ButtonTransparent

   
} from '../../styles/DefaultStyles'

const PR_COLOR = '#0037FB';

export default class Splash1 extends Component{
    
    constructor(props){
        super(props);

        //from the Component
        this.state = {

        }
        this.toSignUp = this.toSignUp.bind(this);
        this.toLogIn = this.toLogIn.bind(this);
        this.tobizAccount = this.tobizAccount.bind(this)
    }

    toSignUp(){
        Actions.SignUp();
        console.log("sign up clicked")
    }

    toLogIn(){
        Actions.LogIn();
        console.log("login clicked")
    }

    tobizAccount(){
        Actions.BizAccount();
        console.log("biz acc clicked");
    }

    async componentDidMount(){

    }

    render(){
        
        return (
            <MainContainer>
                <LogoContainer>
                    <LogoImage source={require('../../../assets/images/logos/logoMain.png')} />
                </LogoContainer>
                
                <ButtonContainer>
                    <ButtonPrimary 
                        onPress={this.toSignUp}
                        activeOpacity={0.7} 
                        >
                        <TextDefault color={'#fff'}>
                           SIGN UP
                        </TextDefault>
                    </ButtonPrimary>

                    <ButtonBlack 
                        onPress={this.toLogIn}
                        activeOpacity={0.7}
                        >
                     <TextDefault color={'#fff'}>
                            LOG IN
                        </TextDefault>

                    </ButtonBlack>

                    <ButtonTransparent 
                        onPress={this.tobizAccount}
                        activeOpacity={0.7}
                        >
                        <TextDefault color={PR_COLOR}>
                        Create a Business Account
                        </TextDefault>
                    </ButtonTransparent>
                    
                </ButtonContainer>
            </MainContainer>
        );
    }
}