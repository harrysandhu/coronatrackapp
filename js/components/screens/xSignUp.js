import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import { 
        SafeAreaView, View } from 'react-native';
 
import { Divider, Icon, Layout, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components';


import {StyleSheet} from 'react-native'


/*Styles*/

const PR_COLOR = '#0037FB';


const BackIcon = (style) => (
  <Icon {...style} name='arrow-back'/>
);



export default class xSignUp extends Component{

    constructor(props){
        super(props);

        //from the Component
        this.state = {}
        this.BackAction = this.BackAction.bind(this)
        this.navigateBack = this.navigateBack.bind(this)
         this.backIcon = this.backIcon.bind(this)
    }

   

    async componentDidMount(){

    }

    BackAction(){
        return(
        <TopNavigationAction icon={this.backIcon} onPress={this.navigateBack}/>
        );
    };
   
    navigateBack(){
        this.props.navigation.goBack();
    }

    backIcon = (style)=> {
        return (
            <Icon name='arrow-back' {...style} width={32} height={32} fill='#000'/>
        )
    }
   

    // BackAction(){
    //      <TopNavigationAction icon={this.backIcon } onPress={this.navigateBack}/>
    // }


    render(){
        
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <TopNavigation title='Sign up' alignment='center' leftControl={this.BackAction()}/>
                <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name='star' width={32} height={32} fill='#3366FF'/>
                    <Text category='h1'>Sign Up</Text>
                </Layout>
            </SafeAreaView>
        );
    }
}