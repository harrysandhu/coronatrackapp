import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import { 
        SafeAreaView, StyleSheet } from 'react-native';
 
import { Divider, Icon, Layout, Button, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components';


/*Styles*/


import {
    MainContainer,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ButtonTransparent

   
} from '../../styles/DefaultStyles'


const PR_COLOR = '#0037FB';
 

export default class xSplash extends Component{


    constructor(props){
        super(props);

        //from the Component
        this.state = {}

    }

   

    async componentDidMount(){

    }

    render(){
        
        return (
           <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
                <MainContainer>

                <ButtonContainer>
                    <ButtonPrimary onPress={() =>{ this.props.navigation.push('BSignUp')}}>
                        <TextDefault color={'#fff'}>BUSINESS ACCOUNT</TextDefault>
                    </ButtonPrimary>
                    <ButtonBlack onPress={() =>{ this.props.navigation.push('SignUp')}}>
                        <TextDefault color={'#fff'}>SIGN UP</TextDefault>
                    </ButtonBlack>
                    <ButtonTransparent onPress={() =>{ this.props.navigation.push('SignUp')}}>
                        <TextDefault color={'#000'}>LOG IN</TextDefault>
                    </ButtonTransparent>
                </ButtonContainer>
                </MainContainer>
           </SafeAreaView>
        );
    }
}