import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';

import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        KeyboardAvoidingView,
        ScrollView,
        Image } from 'react-native';
 

import ImagePicker from 'react-native-image-crop-picker';

import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'

import BizAccountStep1 from '../forms/BizAccountStep1';
import BizAccountStep2 from '../forms/BizAccountStep2';
import PhoneNumberForm from '../forms/PhoneNumberForm';
import VerifyPhone from '../forms/VerifyPhone'
// import AccountInfo from '../forms/AccountInfo'



import {
    BASE_URL,
    RESPONSES
} from '../../functions/helperConstants'

/*Styles*/

import {
    MainContainer,
    FlexView,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ImageSelectorTouchable,
    ProfilePictureImage,
    ProfilePictureUploadView,
    FormGroup,
    InputTF,
    DefaultErrorText,
    LabelText,
    FooterView,

} from '../../styles/DefaultStyles'

import { checkEmailAddress, checkName, checkPhone } from "../../functions/helpers";


/*Import Components*/
import NavBarStage from '../UI/NavBarStage'




export default class BSignUp extends Component{
    constructor(props) {
        super(props);
        this.state ={
            signupId: "",
            viewTitle: 'BUSINESS ACCOUNT',
            imageIsSelected: false,
            imageURI: null,
            emailTF: "",
            emailTFActive: false,
            passwordTF: "",
            passwordTFActive: false,
            nameTF: "",
            nameTFActive: false,
            phoneTF: "",
            phoneExt: "1";
            phoneTFActive : true,
            codeTF: "",
            codeTFActive: true,
            phoneIsValid: false,
            verifyCodeReceived: false,
            verifyCode : null,
            usernameTF: "",
            usernameTFActive:true,
            messageId: null,
            error : {
                errorMessage : null,
                errorStatus: null,
                errorCode: null
            },
            image: {},
            profilePictureIsSelected: false,
            profilePictureImageSource: "",
            profilePictureImageData: null,
            step: 0,

            
        }

        //this.handleSelectImage = this.handleSelectImage.bind(this)
        this.changeState = this.changeState.bind(this)
        this.controlBack = this.controlBack.bind(this)
    }


    getProfilePicture(){
        let {imageIsSelected} = this.state
        if(imageIsSelected){
            return (
                <ProfilePictureImage source={{uri: profilePictureImageSource}} />
            )
        }
        return (
              <ProfilePictureImage source={require('../../../assets/images/ui/defaultAvatar.png')} />
        )
    }


    changeState(obj){
        let oldState = this.state
            for(var key in obj){
                if(oldState.hasOwnProperty(key)){
                    oldState[key] = obj[key];
                    this.setState(oldState);
                }
            }
    }

    controlBack(){
        this.props.navigation.goBack()
    }

    render(){
        
        let {step} = this.state
        console.log("STEP: ", step)
        console.log(this.props)
 //<BizAccountStep1 changeState={this.changeState} state={this.state} nextStep={this.handleNextStep} />
        if(step == 0){
            return (
                <PhoneNumberForm changeState={this.changeState} state={this.state} controlBack={this.controlBack}/>
            )
        }else if(step == 1){
            console.log("step 1, verify phone")
            return (
                <VerifyPhone changeState={this.changeState} state={this.state} controlBack={this.controlBack} />

            )
        }else if(step == 2){
            return (
               <BizAccountStep1 changeState={this.changeState} state={this.state} />
            )
        }else if(step == 3){
            console.log("step 13!")
            return (
               <BizAccountStep1 changeState={this.changeState} state={this.state} />
            )
        }else{
            return (
                <>
                    <View style={{flex: 1}}>
                    <Text>hi 2</Text>
                </View>
                </>
            )
        }

    }
    
}  