


import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';

import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        KeyboardAvoidingView,
        ScrollView,
        Image } from 'react-native';
 


import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'



/*Styles*/

import {
    MainContainer,
    FlexView,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ImageSelectorTouchable,
    ProfilePictureImage,
    ProfilePictureUploadView,
    FormGroup,
    InputTF,
    DefaultErrorText,
    LabelText,
    FooterView,

} from '../../styles/DefaultStyles'

/*Import Components*/
import NavBarCustom from '../UI/NavBarCustom'




export default class BizAccountStep2 extends Component{
    constructor(props){
        super(props);

        this.handleSelectImage = this.handleSelectImage.bind(this)
        this.getProfilePicture = this.getProfilePicture.bind(this)
    }
    async handleSelectImage(){

        try{
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                cropping: true
                }).then(image => {
                    let imagePath = "file://" + image.path;

                    //convert the image into base64
                    RNFS.readFile(imagePath, "base64")
                    .then((res) => {
                        //base64 string
                        let base64DataString = "data:" + image.mime + ';base64,' + res;
                        //console.log(base64DataString)
                        this.props.changeState({
                            profilePictureIsSelected: true,
                            profilePictureImageSource: imagePath,
                            profilePictureImageData: base64DataString
                        });
                    }).catch((RNFSError) =>{
                        console.log(RNFSError);
                    })
                    // throw new Error("error created")
                }).catch(er => {
                    console.log(er);
                })
        }catch(e){
            console.log(e);
            let error = {
                errorMessage: "You need to select a profile picture.",
                errorStatus: "PROFILE_PIC_NOT_SELECTED"
            }
            this.props.changeState({error});
        }
 console.log(this.props.state)
    }

 getProfilePicture(){
        let {profilePictureIsSelected, profilePictureImageSource} = this.props.state
        console.log(this.props.state)
        if(profilePictureIsSelected){
            console.log("here: "+ profilePictureImageSource)
            return (
                <ProfilePictureImage source={{uri: profilePictureImageSource}} />
            )
        }else{
            return (
                <ProfilePictureImage source={require('../../../assets/images/ui/defaultAvatar.png')} />
            )
        }
    }


    render(){

        let {
            viewTitle,
            nameTFActive,
            nameTF, 
            emailTFActive, 
            emailTF, 
            error,
            step,
        } = this.props.state;

        let errorMessage = 
            (error.errorMessage === null) ? "" : error.errorMessage
        
        return (
            <View style={{flex:1}}>  
 <NavBarCustom user={{}} title={viewTitle} backAction={()=>{this.props.changeState({step: 0})}} />
              <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1, background:'#f7f7f7' }}
            >
            <MainContainer>
            <ScrollView
                            contentContainerStyle={{
                                minWidth: "100%"
                            }}>
                <FlexView>
                    <Text>Hello</Text>

                </FlexView>
                </ScrollView>
            
                  <FooterView>
                        <ButtonDarkGrey 
                        onPress={this.props.nextStep}
                        >
                        <TextDefault color={"#fff"}>
                            CONTINUE
                        </TextDefault>
                        </ButtonDarkGrey>
                     </FooterView>
            </MainContainer>  
            </KeyboardAvoidingView> 
            </View>
        )
    }

    
}