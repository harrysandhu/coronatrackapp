import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';
import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        KeyboardAvoidingView,
        ScrollView,
        Image } from 'react-native';
import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'



/*Styles*/

import {
    MainContainer,
    FlexView,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ImageSelectorTouchable,
    ProfilePictureImage,
    ProfilePictureUploadView,
    FormGroup,
    InputTF,
    DefaultErrorText,
    LabelText,
    FooterView,
    InlineFlex

} from '../../styles/DefaultStyles'

/*Import Components*/
import NavBarCustom from '../UI/NavBarCustom'
import NavBarStage from '../UI/NavBarStage'
/*Import Functions*/
// import { 
//         checkEmailAddress, 
//         checkName,
//         checkPhone,
//         getPhoneVerifyCode,
//         startNewUser
//         } from "../../functions/helpers";

import * as Helpers from '../../functions/helpers';

export default class PhoneNumberForm extends Component{
    constructor(props){
        super(props);

        this.processForm = this.processForm.bind(this);
    }

async processForm(){
    let {
        phoneExt,
        phoneTF
    } = this.props.state;
     
    phoneTF = phoneTF
    console.log("phone", phoneTF);
    try{
        await Helpers.checkPhone(phoneTF);
        console.log("check phone passed.")
        let res = await Helpers.getPhoneVerifyCode(phoneExt, phoneTF);
        console.log(res)
        this.props.changeState({step: this.props.state.step + 1, 
        verifyCodeReceived: true,
        messageId: res.MessageID,
        verifyCode: res.code});
    }catch(error){
        this.props.changeState({
            error: error
        })
    }
}




    render(){
        
        let {
            viewTitle,
            phoneTFActive,
            phoneTF,
            nameTFActive, 
            nameTF,
            error,
            step,
        } = this.props.state;
        

        let errorMessage = 
            (error.errorMessage === null) ? "" : error.errorMessage

        return (
            <View style={{flex : 1}}>
                <NavBarStage user={{}} title={viewTitle} handleAction={this.props.controlBack}/>
                
                <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1, background:'#f7f7f7' }}>
                    <MainContainer>
                        <FlexView>
                             <FormGroup>
                                <DefaultErrorText>
                                    {errorMessage}
                                </DefaultErrorText>
                            </FormGroup>



                            <FormGroup>
                    <LabelText size={'14px'} color={'#1A1A1A'}>
                    Phone Number
                 </LabelText>
                <InlineFlex>

                    <LabelText size={'20px'} color={'#101010'}>
                        +1
                    </LabelText>
                    <InputTF keyboardType="number-pad" 
                            active={phoneTFActive} 
                            value={phoneTF}
                            autoCapitalize="none"
                            onFocus={()=>{
                                this.props.changeState({
                                    phoneTFActive: true
                                })
                            }}

                            onBlur={() =>{
                                this.props.changeState({
                                    phoneTFActive: false
                                })
                            }}
                            onChangeText={(value) =>{
                                if(value.length <= 10){
                                    this.props.changeState({
                                        phoneTF: value
                                    })
                                }
                    }}/>
                </InlineFlex> 
                            </FormGroup>
                        </FlexView>

                        <FooterView>
                        <ButtonDarkGrey 
                        onPress={this.processForm}
                        >
                        <TextDefault color={"#fff"}>
                            CONTINUE
                        </TextDefault>
                        </ButtonDarkGrey>
                     </FooterView>
                    </MainContainer>
                </KeyboardAvoidingView>

            </View>
        )
        
    }

}