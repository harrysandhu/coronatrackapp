import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';
import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        KeyboardAvoidingView,
        ScrollView,
        Image } from 'react-native';
import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'



/*Styles*/

import {
    MainContainer,
    FlexView,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ImageSelectorTouchable,
    ProfilePictureImage,
    ProfilePictureUploadView,
    FormGroup,
    InputTF,
    DefaultErrorText,
    LabelText,
    FooterView,
    InlineFlex

} from '../../styles/DefaultStyles'

/*Import Components*/
import NavBarCustom from '../UI/NavBarCustom'
import NavBarStage from '../UI/NavBarStage'
/*Import Functions*/
// import { 
//         checkEmailAddress, 
//         checkName,
//         checkPhone,
//         getPhoneVerifyCode,
//         startNewUser
//         } from "../../functions/helpers";

import * as Helpers from '../../functions/helpers';


export default class BizAccountStep1 extends Component{
    constructor(props){
        super(props);

        this.handleSelectImage = this.handleSelectImage.bind(this)
        this.getProfilePicture = this.getProfilePicture.bind(this)
        this.processForm = this.processForm.bind(this)
    }



async processForm(){
    let {
        nameTF, 
        emailTF, 
        profilePictureImageData,
        profilePictureIsSelected
    } = this.props.state

    
    try{
        // await checkName(nameTF);
        // await checkEmailAddress(emailTF);
        // if(!profilePictureIsSelected){
        //     throw RESPONSES.PROFILE_PIC_NOT_SEL;
        // }
        this.props.changeState({step : this.props.state.step + 1})
    }catch(error){
       this.props.changeState({
            error: error
        })
    }

}
    

    async handleSelectImage(){

        try{
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                cropping: true
                }).then(image => {
                    let imagePath = "file://" + image.path;

                    //convert the image into base64
                    RNFS.readFile(imagePath, "base64")
                    .then((res) => {
                        //base64 string
                        let base64DataString = "data:" + image.mime + ';base64,' + res;
                        //console.log(base64DataString)
                        this.props.changeState({
                            profilePictureIsSelected: true,
                            profilePictureImageSource: imagePath,
                            profilePictureImageData: base64DataString
                        });
                    }).catch((RNFSError) =>{
                        console.log(RNFSError);
                    })
                    // throw new Error("error created")
                }).catch(er => {
                    console.log(er);
                })
        }catch(e){
            console.log(e);
            let error = {
                errorMessage: "You need to select a profile picture.",
                errorStatus: "PROFILE_PIC_NOT_SELECTED"
            }
            this.props.changeState({error});
        }
 console.log(this.props.state)
    }

 getProfilePicture(){
        let {profilePictureIsSelected, profilePictureImageSource} = this.props.state
        console.log(this.props.state)
        if(profilePictureIsSelected){
            console.log("here: "+ profilePictureImageSource)
            return (
                <ProfilePictureImage source={{uri: profilePictureImageSource}} />
            )
        }else{
            return (
                <ProfilePictureImage source={require('../../../assets/images/ui/defaultAvatar.png')} />
            )
        }
    }


    render(){

        let {
            viewTitle,
            nameTFActive,
            nameTF, 
            emailTFActive, 
            emailTF, 
            error,
            step,
        } = this.props.state;

           var errorMessage = 
            (error.errorMessage === null) ? "" : error.errorMessage
        
        return (
            <View style={{flex:1}}>  
             <NavBarStage user={{}} title={viewTitle} />
              <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1, background:'#f7f7f7' }}
            >
            <MainContainer>
              
                        <ScrollView
                            contentContainerStyle={{
                                minWidth: "100%"
                            }}>
                <FlexView>
                    <FormGroup>
                        <DefaultErrorText>
                            {errorMessage}
                        </DefaultErrorText>
                    </FormGroup>

                    <ImageSelectorTouchable 
                        onPress={this.handleSelectImage}
                    >
                        <FlexView justifyContent="center">
                            <ProfilePictureUploadView>
                                {this.getProfilePicture()}
                            </ProfilePictureUploadView>
                            </FlexView>
                    </ImageSelectorTouchable>

                
                     <FormGroup>
                 <LabelText size={'14px'} color={'#1A1A1A'}>
                    Name
                 </LabelText>
                <InputTF keyboardType="default" 
                        active={nameTFActive} 
                         value={nameTF}
                         autoCapitalize="none"
                        onFocus={()=>{
                            this.props.changeState({
                                nameTFActive: true
                            })
                        }}

                        onBlur={() =>{
                            this.props.changeState({
                                nameTFActive: false
                            })
                        }}
                         onChangeText={(value) =>{
                             this.props.changeState({
                                nameTF: value
                             })
                         }}

                        />
                     </FormGroup>


                      <FormGroup>
                 <LabelText size={'14px'} color={'#1A1A1A'}>
                    Email Address
                 </LabelText>
                <InputTF keyboardType="email-address" 
                        active={emailTFActive} 
                         value={emailTF}
                         autoCapitalize="none"
                        onFocus={()=>{
                            this.props.changeState({
                                emailTFActive: true
                            })
                        }}

                        onBlur={() =>{
                            this.props.changeState({
                                emailTFActive: false
                            })
                        }}
                         onChangeText={(value) =>{
                             this.props.changeState({
                                emailTF: value
                             })
                         }}

                        />
                     </FormGroup>

                </FlexView>
                </ScrollView>
                     <FooterView>
                        <ButtonDarkGrey 
                        onPress={this.processForm}
                        >
                        <TextDefault color={"#fff"}>
                            CONTINUE
                        </TextDefault>
                        </ButtonDarkGrey>
                     </FooterView>

            </MainContainer>  
            </KeyboardAvoidingView> 
            </View>
        )
    }

    
}