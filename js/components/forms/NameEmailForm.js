import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';
import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        KeyboardAvoidingView,
        ScrollView,
        Image } from 'react-native';
import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'



/*Styles*/

import {
    MainContainer,
    FlexView,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ImageSelectorTouchable,
    ProfilePictureImage,
    ProfilePictureUploadView,
    FormGroup,
    InputTF,
    DefaultErrorText,
    LabelText,
    FooterView,
    InlineFlex

} from '../../styles/DefaultStyles'

/*Import Components*/
import NavBarCustom from '../UI/NavBarCustom'
import NavBarStage from '../UI/NavBarStage'
/*Import Functions*/
// import { 
//         checkEmailAddress, 
//         checkName,
//         checkPhone,
//         getPhoneVerifyCode,
//         startNewUser
//         } from "../../functions/helpers";

import * as Helpers from '../../functions/helpers';



export default class NameEmailForm extends Component{
    constructor(props) {
        super(props);

        this.processForm = this.processForm.bind(this)    
    }



    async processForm(){
        let {
            nameTF,
            emailTF
        } = this.props.state;

        try{
            await Helpers.checkName(nameTF)
            console.log("checkname pass");
            await Helpers.checkEmail(emailTF)
            console.log("checkemail passed")
            this.props.changeState({
                step : this.props.state.step +1
            })

        }catch(error){
            console.log(error)
            this.props.changeState({
                error:error
            })
        }
        
    }
    
}