

/*
   {
    business name 
    email
} 
{profile picture, username}

finish business account 

personal account- > use review the changes 

-> finish signup
*/


import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';
import { Button, 
        Text, 
        View, 
        TouchableOpacity,
        TouchableWithoutFeedback,
        KeyboardAvoidingView,
        ScrollView,
        Image } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import {StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux'



/*Styles*/
import {
    MainContainer,
    FlexView,
    LogoContainer,
    ButtonContainer,
    LogoImage,
    GreyText,
    TextDefault,
    SplashContent,
    ButtonPrimary,
    ButtonDarkGrey,
    ButtonBlack,
    ImageSelectorTouchable,
    ProfilePictureImage,
    ProfilePictureUploadView,
    FormGroup,
    InputTF,
    DefaultErrorText,
    LabelText,
    FooterView,

} from '../../styles/DefaultStyles'
/*Import Components*/
import NavBarStage from '../UI/NavBarStage'
/*Import Functions*/
import { 
        checkEmailAddress, 
        checkName,
        checkPhone,
        checkPassword,
        signUpUser,
        } from "../../functions/helpers";

    
import  {
    RESPONSES
} from '../../functions/helperConstants'


export default class EmailForm extends Component{
    constructor(props){ 
        super(props);


        this.processForm = this.processForm.bind(this)
    }


//signup user
// async processForm(){
//     let {
//         phoneTF,
//         emailTF,
//         passwordTF
//     } = this.props.state

//     let user = {
//         phone: phoneTF,
//         email: emailTF,
//         password: passwordTF
//     };

//     console.log(user)
//     try{
//         await checkEmailAddress(emailTF)
//         console.log("aight")
//         await checkPassword(passwordTF)
//         console.log("aight")
//         let res = await signUpUser(user);

//         if(res.success){
//             //set asyncstorage

//             //redirect to home
//         }else{
//             throw RESPONSES.ERR_SYSTEM
//         }
    
//         this.props.changeState({step : this.props.state.step + 1})
//     }catch(error){
//        this.props.changeState({
//             error: error
//         })
//     }

// }









    render(){

        let {
            viewTitle,
            emailTFActive, 
            emailTF, 
            passwordTFActive,
            passwordTF,
            error,
            step,
        } = this.props.state;

           var errorMessage = 
            (error.errorMessage === null) ? "" : error.errorMessage
        
        return (
            <View style={{flex:1}}>  
             <NavBarStage user={{}} title={viewTitle} />
              <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1, background:'#f7f7f7' }}
            >
            <MainContainer>
              
                        <ScrollView
                            contentContainerStyle={{
                                minWidth: "100%"
                            }}>
                <FlexView>
                    <FormGroup>
                        <DefaultErrorText>
                            {errorMessage}
                        </DefaultErrorText>
                    </FormGroup>


                    {/**EMAIL ADDRESS */}
                      <FormGroup>
                 <LabelText size={'14px'} color={'#1A1A1A'}>
                    Email Address
                 </LabelText>
                <InputTF keyboardType="email-address" 
                        active={emailTFActive} 
                         value={emailTF}
                         autoCapitalize="none"
                        onFocus={()=>{
                            this.props.changeState({
                                emailTFActive: true
                            })
                        }}

                        onBlur={() =>{
                            this.props.changeState({
                                emailTFActive: false
                            })
                        }}
                         onChangeText={(value) =>{
                             this.props.changeState({
                                emailTF: value
                             })
                         }}

                        />
                     </FormGroup>
                    

                {/** PASSWORD */}

                      <FormGroup>
                 <LabelText size={'14px'} color={'#1A1A1A'}>
                    Password
                 </LabelText>
                <InputTF keyboardType="default"
                        secureTextEntry={true} 
                        active={passwordTFActive} 
                         value={passwordTF}
                        onFocus={()=>{
                            this.props.changeState({
                                passwordTFActive: true
                            })
                        }}

                        onBlur={() =>{
                            this.props.changeState({
                                passwordTFActive: false
                            })
                        }}
                         onChangeText={(value) =>{
                             this.props.changeState({
                                passwordTF: value
                             })
                         }}

                        />
                     </FormGroup>


                </FlexView>
                </ScrollView>
                     <FooterView>
                        <ButtonDarkGrey 
                        onPress={this.processForm}
                        >
                        <TextDefault color={"#fff"}>
                            CONTINUE
                        </TextDefault>
                        </ButtonDarkGrey>
                     </FooterView>

            </MainContainer>  
            </KeyboardAvoidingView> 
            </View>
        )
    }

    
}