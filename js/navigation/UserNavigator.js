import React, {Component} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';


import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {View} from 'react-native'
import MapScreen from '../components/screens/MapScreen'
import ReportHome from '../components/screens/ReportHome'
import HelpScreen from '../components/screens/HelpScreen'
import {NavButtonImage} from '../styles/DefaultStyles'
import Icon, { Ionicons  } from 'react-native-vector-icons/Ionicons';




const Tab = createBottomTabNavigator();



export default class UserNavigator extends Component{

      constructor(props) {
        super(props);
        this.state = {}
    }

    render(){
        return (
            <Tab.Navigator 
            navigationOptions = {{
      gesturesEnabled: false
    }}

                 screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === 'ReportHome') {
              iconName = focused
                ?  'md-clipboard'
                : 'md-clipboard';
            } else if (route.name === 'MapScreen') {
              iconName = focused ? 'md-globe' : 'md-globe';
            }else if(route.name === "HelpScreen"){
              iconName = "md-information-circle"
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={23} color={color} />
          },
        })}
      
        tabBarOptions={{
          activeTintColor: '#E12727',
          inactiveTintColor: 'gray',
          style: {
            marginTop:'-2%',
        paddingLeft:'25%',
        paddingRight:'25%',
           backgroundColor: '#f9f9f9'
          }
          
        }}
            >
                <Tab.Screen name="ReportHome" component={ReportHome} options={{ headerShown: false, tabBarLabel: 'Report' }}/>
                <Tab.Screen name="MapScreen" component={MapScreen}  options={{ headerShown: false, tabBarLabel: 'Map' }}/>
                <Tab.Screen name="HelpScreen" component={HelpScreen}  options={{ headerShown: false, tabBarLabel: 'Info' }}/>
                {/* <Stack.Screen name="BSignUp" component={BSignUp} /> */}
            </Tab.Navigator>
        );
    }
}
